public class PhoneBuilder {
    private String modelName;
    private String os;
    private int ram;
    private String processor;
    private double screenSize;
    private int battery;
    private int camera;
    private int numberSims;

    public PhoneBuilder setModelName(String modelName) {
        this.modelName = modelName;
        return this;
    }

    public PhoneBuilder setOs(String os) {
        this.os = os;
        return this;
    }

    public PhoneBuilder setRam(int ram) {
        this.ram = ram;
        return this;
    }

    public PhoneBuilder setProcessor(String processor) {
        this.processor = processor;
        return this;
    }

    public PhoneBuilder setScreenSize(double screenSize) {
        this.screenSize = screenSize;
        return this;
    }

    public PhoneBuilder setBattery(int battery) {
        this.battery = battery;
        return this;
    }

    public PhoneBuilder setCamera(int camera) {
        this.camera = camera;
        return this;
    }

    public PhoneBuilder setNumberSims(int numberSims) {
        this.numberSims = numberSims;
        return this;
    }


    public Phone getPhone() {
        return new Phone(modelName, os, ram, processor, screenSize, battery, camera, numberSims);
    }
}
