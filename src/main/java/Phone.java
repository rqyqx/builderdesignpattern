public class Phone {
    private String modelName;
    private String os;
    private int ram;
    private String processor;
    private double screenSize;
    private int battery;
    private int camera;
    private int numberSims;

    public Phone(String modelName, String os, int ram, String processor, double screenSize, int battery, int camera, int numberSims) {
        this.modelName = modelName;
        this.os = os;
        this.ram = ram;
        this.processor = processor;
        this.screenSize = screenSize;
        this.battery = battery;
        this.camera = camera;
        this.numberSims = numberSims;
    }

    @Override
    public String toString() {
        return "Phone configurations: " + "model - '" + modelName + '\'' + ", os - '" + os + '\'' + ", processor - '" + processor + '\'' + ", ram = " + ram + " GB" + ", screenSize = " + screenSize + ", battery = " + battery + " MPh" + ", camera = '" + camera + " MP" + '\'' + ", numberOfSims = " + numberSims;
    }
}
