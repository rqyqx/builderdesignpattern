public class Main {
    public static void main(String[] args) {
        Phone p = new Phone("Samsung s10","Android",8,"Qualcomm snapdragon 865",5.5,3100,20,2);
        Phone ph = new PhoneBuilder().setModelName("Iphone 11").setOs("IOS").setProcessor("A12 bionic").setRam(3).setScreenSize(5.2).setBattery(2800).setCamera(16).setNumberSims(1).getPhone();
        System.out.println(p);
        System.out.println(ph);
    }
}
